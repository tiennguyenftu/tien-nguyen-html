# Knowledge used in this project
## General:
-Use almost HTML  
-A little bit CSS, Javascript  

### Header:
-Use custom fonts for logo  
-Use styled nav bar  

### Footer:
-Use special characters (copyright, trademark)  
-Use custom social buttons  

## Home Page
-Use headings  
-Embed codes  
-Insert images  
-Link to pages within the sites (internal links)  
-List items  

## CSS Page
-Embed videos from Vimeo  
-Use external anchor links  

## Javascript Page
-Use float image  
-Embed video, audio  

## React Page
-Center images  
-Use table  
-Use description list  
-Use list items  

## Meteor page
-Using tab bar with Javascript  


